#!/usr/bin/perl
# $Id: submit.pl,v 1.8 2013-04-20 17:31:13 mw Exp $

use CGI qw/:standard/;
use CGI::Carp qw/fatalsToBrowser/;
use DBI;
use Module::Load;
load 'config.pl';

my $quote = param('quote');
my $nick = param('nick');
my $context = param('context');
my $comment = param('comment');

my $dbh = DBI->connect($config::dbi,"","")
	or die $dbh->errstr;

print header,
      start_html(-title => 'Quote submitted',
                 -meta => { robots => 'noindex,nofollow' }),
      h1('Quote submitted'),
      a({href => "."}, "Go back");

my $sth = $dbh->prepare(qq{
	INSERT INTO quotes (nick, quote, submit_time, comment, out_of_context)
	VALUES (?, ?, ?, ?, ?);
}) or die $dbh->errstr;

my $error = p(b('There was an error adding your Quote.',
		'Please make sure you submitted both your name and the quote.'));

$nick =~ s/(^\d*|\s)//g;
$quote =~ s/\s*$//mg;
$comment =~ s/\s+/ /mg;
$context = defined($context) && $context =~ /on/i;
if (defined ($nick) && $nick ne '' &&
    defined($quote) && $quote ne '') {
	if ($sth->execute($nick, $quote, time, $comment, defined($context)) {
		print p({ id => "success" }, b('Your quote was added'));
	} else {
		die $dbh->errstr;
	}
} else {
	print $error;
}

print end_html;
