#!/usr/bin/perl -w
# $Id: config.pl,v 1.7 2014-06-18 16:02:58 mw Exp $
package config;
use CGI qw/Link/;

$CGI::DISABLE_UPLOADS = 1;
$CGI::POST_MAX = 1024 * 2; # max 2k posts

my $DBFILE = '/var/db/cgi-bin/quotes/quotes.db3';
our $dbi = "dbi:SQLite:dbname=$DBFILE";

our %rss = ( # settings 
	url => q<http://wzff.de/quotes/>,
	title => '#imgurians quotes',
	desc => 'Quotes from #imgurians on IRCnet',
	limit => 20, # max this many entries in the feed
);

my $url_pattern = qr{(
(?:https?://|ftp://|news://|git://|mailto:|file://|www\.)
[\w\-\@;\/?:&=%\$_.+!*\x27(),~#]+[\w\-\@;\/?&=%\$_+!*\x27()~]
)}x; # admittedly stolen from url-select; Cheers to Bert Muennich

our %page = (
	rewriting => 0, # set to 0 if you can't control URL rewriting.
	permalink_prefix => '', # use ?id= if you can't control URL rewriting ## DEPRECATED
	page_prefix => 'p', # use ?page= if you can't control URL rewriting ## DEPRECATED
	landing_page => '.', # set to "index.pl" if index.pl isn't in the httpd's default index file name list ## DEPRECATED
	url_pattern => $url_pattern,

	# HTML <head> items
	title => '#imgurians quotes page',
	head => [
		Link({ rel => 'stylesheet', type => 'text/css', href => 'shiny.css' }),
		Link({ rel => 'stylesheet', type => 'text/css', href => 'shiny_mobile.css', media => 'only screen and (max-device-width: 960px)' }),
		Link({ rel => 'alternate', type => 'application/rss+xml', href => 'rss.pl', title => 'Feed' })
	],
	meta => {
		robots => 'noindex,nofollow',
		viewport => 'width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;'
	},
	num_quotes => 20, # number of quotes per page
	strftime_string => '%Y-%m-%d %T', # time format to use, see strftime(3)
);

1
