#!/usr/bin/perl
# $Id: fortune.pl,v 1.5 2013-04-20 00:18:42 mw Exp $

use CGI qw/:standard/;
use CGI::Carp qw/fatalsToBrowser/;
use DBI;
use Module::Load;
load 'config.pl';

my $dbh = DBI->connect($config::dbi,"","")
	or die $dbh->errstr;

$stmt = $dbh->prepare("SELECT quote FROM quotes ORDER BY id DESC")
	or die $dbh->errstr;

$stmt->execute;

charset 'utf-8';
print header('text/plain');

while (my @row = $stmt->fetchrow_array) {
	my $q = shift @row or next;
	$q =~ s/^\%//gm;
	my @q = grep /\S/, split /\n/, $q;

	print join ("\n", @q), "\n%\n";
}
