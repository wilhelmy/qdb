#!/usr/bin/perl
# $Id: rss.pl,v 1.10 2014-06-18 16:00:29 mw Exp $

use DBI;
use CGI qw/:standard/;
use XML::RSS;
use DateTime;
use DateTime::Format::Epoch;
use DateTime::Format::Mail;
use Module::Load;
use utf8;
load 'config.pl';

my $dbh = DBI->connect($config::dbi,"","") or die $dbh->errstr;

charset 'utf-8';

my $rss = XML::RSS->new(version => '2.0');

my $epoch = DateTime::Format::Epoch->new(
	epoch => DateTime->new(
		year => 1970,
		month => 1,
		day => 1,
		time_zone => "UTC"
	)
);

my $mail = DateTime::Format::Mail->new;

my $sth = $dbh->prepare("SELECT MAX(submit_time) FROM quotes");
$sth->execute;
my @row = $sth->fetchrow_array || [0];

$rss->channel(
	title => $config::rss{title},
	description => $config::rss{description},
	link => $config::rss{url},
	pubDate => $mail->format_datetime($epoch->parse_datetime($row[0])),
	lastBuildDate => $mail->format_datetime(DateTime->now( time_zone => 'UTC' )),
);

my $sth = $dbh->prepare("SELECT id, quote FROM quotes ORDER BY id DESC LIMIT $config::rss{limit};")
	or die $dbh->errstr;

$sth->execute;

while (my $row = $sth->fetchrow_hashref) {
	utf8::decode $row->{quote};
	$row->{quote} =~ m/(.{1,80})/;
	my $title = "#$row->{id} - $1";
	my $quote = escapeHTML $row->{quote};
	$quote=~s/[\x00-\x09\xb\xc\xe\xf]//g;
	$rss->add_item(
		title => $title,
		link => $config::rss{url} . "#q" . $row->{id},
		permaLink => $config::rss{url} . $config::page{permalink_prefix} . $row->{id},
		description => "<pre>$quote</pre>",
	);
}

print header("application/xml"), $rss->as_string;
