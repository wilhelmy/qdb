#!/usr/bin/perl
# $Id: index.pl,v 1.29 2014-06-18 16:02:58 mw Exp $

use CGI qw/:standard -oldstyle_urls/;
use CGI::Carp qw/fatalsToBrowser/;
use DBI qw/:sql_types/;
use Module::Load;
use POSIX qw/strftime/;
use utf8;
load 'config.pl';

$CGI::Q->import_names('P'); # import all CGI parameters into the P namespace to us some typing later

sub url_to_link {
	my ($quote) = @_;
	$quote =~ s;$config::page{url_pattern};<a href="$1">$1</a>;gm;
	return $quote;
}

# Total number of quotes in the database
# FIXME this value is in one of the views already.
sub total {
	my ($dbh) = @_;

	my $where = '';
	$where = "WHERE quote LIKE ?" if $P::q;
	my $stmt = $dbh->prepare("SELECT COUNT(*) AS count FROM quotes $where")
		or die $dbh->errstr;

	$stmt->bind_param(1, "%$P::q%") if $P::q;
	$stmt->execute;
	while (my $row = $stmt->fetchrow_hashref) {
		return $row->{count} 
	}
	return 0;
}

sub page_url {
	my ($page) = @_;
	my $rewriting = $config::page{'rewriting'};
	my $landing_page = $config::page{'landing_page'};
	my @param = ();

	if ($rewriting) {
		return $page == 1 ? $landing_page : $page;
		# FIXME &view, &q
	} else {
		unshift @param, "q=$P::q" if $P::q;
		unshift @param, "page=$page" unless $page == 1;
		unshift @param, "view=$P::view" if defined $P::view;
		my $r = "?". join '&', @param;
		return $r eq '?' ? "." : $r;
	}
}

sub permalink {
	my ($id) = @_;
	return $config::page{permalink_prefix} . $id;
}

sub paginator {
	my ($page, $total) = @_;

	my $nquotes = $config::page{num_quotes};

	@_ = map {
		$_ == $page ? $_ : a({-href => page_url $_}, $_);
	} (1..$total/$nquotes+1);
	
	unshift @_, a({ -href => page_url $page-1 }, "&lt; Prev") if $page > 1;
	push    @_, a({ -href => page_url $page+1 }, "Next &gt;") if $page < $total/$nquotes;

	return ul map { li $_ } @_;
}

sub page_header {
	print header(-status => shift),
	      start_html(-title => $config::page{title},
			 -meta  => $config::page{meta},
			 -head  => $config::page{head}),
	      h1(a({ -href => '.', -id => 'home', -title => 'All quotes'}, $config::page{title})),
	      ul({ id => "navigation" },
		li(a({href => 'submit.html', -title => "Submit a quote" }, "Submit")),
		li(a({href => 'rss.pl', -title => "RSS feed"}, "RSS feed")),
		li(a({href => 'http://wzff.de/faq/#tard', -title => "How to vote on quotes" }, "Voting")),
		);
}

sub page_footer {
	print end_html;
}


sub page_not_found {
	page_header "404 Not Found";
	
	print div({-id => 'message'},
		h1("404 Not Found"),
		p("The page you've been trying to reach could not be found."),
		p("Please try again at a different location") );

	page_footer;
}

sub sort_form {
	my @options = ['newest', 'oldest', 'best', 'worst'];

	print start_form(-method => 'GET', -id => 'sort-form'),
	    div({-id => 'sort-container'},
	      "Sort by: ",
	      popup_menu(
	        -id => 'sort-dropdown',
	        -name => 'view',
	        -onchange => 'this.form.submit()',
	        -values => @options)),
	    div({-id => 'search-container'},
	      input({-type  => 'text',
	             -id    => 'search',
	             -name  => 'q',
	             -value => $P::q || '',
	             -placeholder => "Search…"}),
	      submit(-name  => '', -label => 'OK')),
	      end_form;
}

sub display_single {
	my ($dbh, $id) = @_;
	my $stmt;

	$stmt = $dbh->prepare("SELECT * FROM quote_display WHERE id = ?")
		or die $dbh->errstr;
	$stmt->bind_param(1, $id, SQL_INTEGER);

	return $stmt;
}

sub display_multiple {
	my ($dbh, $view, $page, $nquotes, $total) = @_;
	my $order = 'id DESC'; # default: newest first
	my $where = ""; # default: no where clause
	
	$view = lc $view;
	if ($view eq 'best') {
		$order = 'rating DESC, id DESC';
	} elsif ($view eq 'worst') {
		$order = 'rating ASC, id DESC';
	} elsif ($view eq 'oldest') {
		$order = 'id ASC';
	}

	$where = "WHERE quote LIKE ? COLLATE NOCASE" if $P::q;

	page_not_found and exit if $page < 1 || $page > $total/$nquotes+1;
	
	$stmt = $dbh->prepare("SELECT * FROM quote_display $where ORDER BY $order LIMIT ? OFFSET ?")
		or die $dbh->errstr;

	my $n = 1;
	$stmt->bind_param($n++, "%$P::q%") if $P::q;

	$stmt->bind_param($n++, $nquotes            , SQL_INTEGER);
	$stmt->bind_param($n++, $nquotes * ($page-1), SQL_INTEGER);

	return $stmt;
}

return 1 if caller;

my $dbh = DBI->connect($config::dbi,"","")
	or die $dbh->errstr;

my $stmt;
my $page;
my $total = total $dbh;
my $nquotes = $config::page{num_quotes};

if (defined $P::id) {
	$stmt = display_single $dbh, int $P::id;
} else {
	$page = (defined($P::page) && ($P::page ne '')) ? int $P::page : 1;
	$stmt = display_multiple $dbh, $P::view, $page, $nquotes, $total;
}

charset 'utf-8';

page_header;
sort_form unless defined $P::id;

print '<div id="quotes">';

$stmt->execute;

while (my $row = $stmt->fetchrow_hashref) {
	my $time = strftime($config::page{'strftime_string'}, gmtime $row->{"submit_time"});
	my $user = escapeHTML $row->{"nick"};
	my $quote = url_to_link escapeHTML $row->{"quote"};
	my $comment = escapeHTML $row->{"comment"};
	my $qid = $row->{'id'};
	print div({-id => "q$qid", -class => $row->{out_of_context} ? 'quote out-of-context' : 'quote'},
		$comment && div({-class => 'comment'}, $comment),
		pre({-class => 'quotetext'}, $quote),
		div({-class => 'timestamp'}, small(
			a({ -href => permalink $qid, -title => "Permalink", -class => 'permalink' }, "#$qid"),
			"submitted by $user on $time ",
			span({-class => 'votes'},
			  span({-class => 'upvote',   -title => 'Upvotes'  }, $row->{'pos'}  || 0),
			  span({-class => 'meh',      -title => 'Meh'      }, $row->{'zero'} || 0),
			  span({-class => 'downvote', -title => 'Downvotes'}, $row->{'neg'}  || 0)),
			)));
}

print '</div>';

if (defined $page && $total > $nquotes) {
	print div({-id => 'paginator'}, paginator($page, $total)) 
}

print div({ -id => "disclaimer" }, "Copyright &copy; 2013 by Ente Analytics, Inc. &mdash; Timestamp stripping JS stolen from umlaut.hu &mdash; CSS and graphics by Gargaj");

page_footer;
