-- $Id: makedb.sql,v 1.1 2013-09-29 14:39:02 mw Exp $
--
-- Use this file to create or update the database file:
--  sqlite3 foo,db < makedb.sql
--

pragma foreign_keys=on;
pragma recursive_triggers=on;

-- initialise the schema
.read schema.sql
-- initialise the cache
.read init.sql
-- delete old crap
--.read deprecated.sql
