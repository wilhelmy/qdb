-- $Id: schema.sql,v 1.9 2013-09-29 21:43:31 mw Exp $
--
-- NOTE: Whenever you write to the votes table, you should enable foreign keys 
--       Foreign key constraints verification is a per-connection option in SQLite. 
--       Syntax: PRAGMA foreign_keys = ON;
--       Whenever you write to the database, make sure to also enable recursive_triggers;

CREATE TABLE IF NOT EXISTS quotes(
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	nick VARCHAR(15) NOT NULL,
	comment VARCHAR(300),
	quote TEXT NOT NULL,
	submit_time INTEGER NOT NULL,
	out_of_context BOOLEAN NOT NULL
);

-- This schema mostly makes sense when voting is done via eggdrop
-- OTOH it could be possible to just extend the egg_handle field and store a
-- cookie inside..
CREATE TABLE IF NOT EXISTS votes(
	egg_handle VARCHAR(15) NOT NULL,
	q_id INTEGER NOT NULL,
	value INTEGER NOT NULL DEFAULT 0,
	FOREIGN KEY (q_id) REFERENCES quotes(id) ON DELETE CASCADE,
	CONSTRAINT range CHECK (value > -2 AND value < 2) -- can't vote other things than -1, 0, 1
);

-- This is probably somewhat inefficient.
CREATE UNIQUE INDEX IF NOT EXISTS vote_once_per_quote ON votes(egg_handle, q_id);

-- Perform a pivot operation on the votes table. The join with quotes is to
-- help populate quotes where nobody has voted yet. This is mostly a helper for
-- populating the vote_cache 
DROP VIEW IF EXISTS vote_display_nocache;
CREATE VIEW vote_display_nocache AS
	SELECT id AS q_id,
		-- these are undefined if nobody has voted on them yet
		COALESCE(SUM(CASE WHEN value = -1 THEN 1 ELSE 0 END), 0) AS neg,
		COALESCE(SUM(CASE WHEN value =  0 THEN 1 ELSE 0 END), 0) AS zero,
		COALESCE(SUM(CASE WHEN value =  1 THEN 1 ELSE 0 END), 0) AS pos
	FROM quotes LEFT JOIN votes ON id = q_id
	GROUP BY id;

-- the above plus rating.
DROP VIEW IF EXISTS vote_rating_nocache;
CREATE VIEW vote_rating_nocache AS
	SELECT *, (pos*1.0-neg)/(neg+zero+1) AS rating FROM vote_display_nocache;

-- cache for the vote sums in vote_display
DROP TABLE IF EXISTS vote_cache; -- just in case, will repopulate it later
CREATE TABLE vote_cache (
	q_id    INTEGER NOT NULL,
	neg     INTEGER NOT NULL DEFAULT 0,
	zero    INTEGER NOT NULL DEFAULT 0,
	pos     INTEGER NOT NULL DEFAULT 0,
	rating  FLOAT   NOT NULL DEFAULT 0,
	FOREIGN KEY (q_id) REFERENCES quotes(id) ON DELETE CASCADE
);

-- as soon as somebody votes, update the entry of the vote cache for that
-- particular quote
DROP TRIGGER IF EXISTS vote_cache_insert;
CREATE TRIGGER vote_cache_insert
	AFTER INSERT ON votes FOR EACH ROW BEGIN
		DELETE FROM vote_cache WHERE q_id = NEW.q_id;
		INSERT INTO vote_cache (q_id, neg, zero, pos, rating)
			SELECT q_id, neg, zero, pos, rating
			FROM vote_rating_nocache r
			WHERE r.q_id = NEW.q_id;
	END;

-- same for deleted votes, which should only happen if a quote is deleted.
DROP TRIGGER IF EXISTS vote_cache_delete;
CREATE TRIGGER vote_cache_delete
	AFTER DELETE ON votes FOR EACH ROW BEGIN
		DELETE FROM vote_cache WHERE q_id = OLD.q_id;
		INSERT INTO vote_cache (q_id, neg, zero, pos, rating)
			SELECT q_id, neg, zero, pos, rating
			FROM vote_rating_nocache r
			WHERE r.q_id = OLD.q_id;
	END;

-- no trigger for "vote_cache_update" yet, I'm not sure it's needed.

-- use the cache here now
DROP VIEW IF EXISTS quote_display;
CREATE VIEW quote_display AS
	SELECT * FROM quotes LEFT JOIN vote_cache ON id = q_id;
