#!/usr/bin/tclsh
#
# Voting on quotes for our qdb-like website via eggdrop
#
# $Id: q_voting.tcl,v 1.12 2013-09-29 22:25:21 mw Exp $

package require sqlite3

# name of the namespace for this script. You can change it, if you want or if
# it collides with something else, however unlikely that may be.
set ns ::q_voting

namespace eval $ns {
	# Command prefix to use in public channels. Should be a single character.
	variable prefix !

	# Location of the database file. Will be different for you.
	variable dbfile "/var/db/cgi-bin/quotes/quotes.db3"

	# Declare v_command_mapping as namespace-local variable before invoking
	# "array set" below. Not entirely sure if this is required, but it
	# can't hurt.
	variable v_command_mapping
	
	# mapping for command <-> vote value
	# you can e.g. add commands in different languages here
	array set v_command_mapping {
		upvote +1
		downvote -1
		indifferent 0
		ok 0
		meh 0
		+ +1
		- -1
		rulez +1
		sucks -1
	}

	# "bind cron" time interval parameter. See the eggdrop manual for more information.
	# The default is to poll every minute. Set to "" to disable polling for new quotes.
	variable poll_interval "*/1 * * * *"

	# the channel information about new quotes gets sent to
	variable poll_target #imgurians

	# you probably don't want to touch these.
	variable ns $ns
	variable disable 0

	variable translations; #FIXME use a language file..?
	array set translations [list \
		unidentified	"Please identify with me. See http://wzff.de/faq/#tard" \
		invalid		"Invalid quote ID." \
		unknown		"Unknown error" \
		nosuchquote	"No such quote" \
		alreadyvoted	"You already voted" \
		baseurl		"http://wzff.de/quotes/"
	]

# helper procedure for using multiple namespace variables
proc vars args {
	uplevel 1 [list foreach v $args { variable $v }]
}

# This procedure must be manually invoked via .tcl q_voting::reopen_db if the database is moved.
# Alternatively, just reload the script, which will automatically reopen the database. (.tcl source scripts/q_voting.tcl)
# While messing with the database, use .set q_voting::disable 1 to disable database access in this script until you're done.
# Afterwards, either reload it or .set q_voting::disable 0 again.
proc reopen_db { } {
	vars ns dbfile disable

	if {$disable} return

	if {[llength [info commands ${ns}::db]]} {
		db close
	}

	sqlite3 ${ns}::db $dbfile
	#init_db
	db eval {PRAGMA foreign_keys=on;}
}

# return the vote value (-1, 0, 1 depending on the command that was used)
# commands can be configured in the v_command_mapping array above
proc value {} {
	global lastbind
	vars prefix v_command_mapping

	set lookup [string trimleft $lastbind $prefix]

	if {[info exists v_command_mapping($lookup)]} {
		return $v_command_mapping($lookup)
	}

	error "Tried to vote '$lastbind' but there is no value in v_command_mapping assigned to it! Please edit the q_voting script."
}

# returns the quote ID or -1 if invalid
proc scan_qid {text} {
	if {([scan $text "#%i" qid] > 0 || [scan $text "%i" qid] > 0) && $qid > 0} {
		return $qid
	}
	return -1;
}

proc vote {handle text} {
	variable translations

	if {$handle == "*"} {
		return $translations(unidentified)
	}

	if {[set q_id [scan_qid $text]] < 1} {
		return $translations(invalid)
	}

	set value [value]
	if {$value < -1 || $value > 1} {
		# better safe than sorry, DB schema will catch this too, though
		error "Out of range"
	}

	if [catch {db eval {
		INSERT INTO votes(egg_handle, q_id, value) VALUES (:handle, :q_id, :value);
	}}] {
		set rv $translations(unknown)
		db eval { SELECT 1 FROM votes WHERE q_id = :q_id AND egg_handle = :handle } {
			set rv $translations(alreadyvoted)
		}
		db eval { SELECT COUNT(*) AS c FROM quotes WHERE id = :q_id } {
			if {$c == 0} {
				set rv $translations(nosuchquote)
			}
		}
		return $rv
	} else {
		db eval { SELECT id, pos, zero, neg FROM quote_display WHERE id = :q_id } {
			return "No. (Voted on quote #$id (Upvotes: $pos, Meh: $zero, Downvotes: $neg): $translations(baseurl)$id)"
		}
		return "No."
	}
}

# public message handler for voting
proc pubvote {nick uhost handle chan text} {
	variable disable
	if {$disable} return

	puthelp "PRIVMSG $chan :$nick: [vote $handle $text]"
}

# private message handler for voting
proc msgvote {nick uhost handle text} {
	variable disable
	if {$disable} return

	puthelp "PRIVMSG $nick :[vote $handle $text]"
}

# DCC message handler for voting
proc dccvote {handle idx text} {
	variable disable
	if {$disable} return

	putdcc $idx [vote $handle $text]
}

# bind all of the mappings to one of the votes commands, depending on their
# context (i.e. DCC, MSG or PUB)
foreach k [array names v_command_mapping] {
	bind pub - ${prefix}$k ${ns}::pubvote
	bind msg -          $k ${ns}::msgvote
	bind dcc -          $k ${ns}::dccvote
}

proc qinfo {nick uhost handle chan text} {
	variable translations

	set text [string trim $text]
	set code {
		puthelp "PRIVMSG $chan :$nick: Quote #$id (Upvotes: $pos, Meh: $zero, Downvotes: $neg): $translations(baseurl)$id"
		return
	}

	if {$text eq ""} {
		db eval { SELECT id, pos, zero, neg FROM quote_display ORDER BY RANDOM() LIMIT 1 } $code
	}

	if {[set q_id [scan_qid $text]] < 1} {
		db eval { SELECT id, pos, zero, neg FROM quote_display WHERE nick = :text COLLATE NOCASE ORDER BY RANDOM() LIMIT 1 } $code
		puthelp "PRIVMSG $chan :$nick: Invalid quote ID."
		return
	}
	db eval { SELECT id, pos, zero, neg FROM quote_display WHERE id = :q_id } {
		puthelp "PRIVMSG $chan :$nick: Quote #$id (Upvotes: $pos, Meh: $zero, Downvotes: $neg): $translations(baseurl)$id"
		return
	}
	puthelp "PRIVMSG $chan :$nick: No such quote."
}

bind pub - ${prefix}q ${ns}::qinfo

# last poll in seconds since 1970
variable poll_last

proc poll_new {minute hour day month weekday} { 
	vars ns disable poll_last poll_target translations

	if {$disable} return

	db eval { SELECT nick, id FROM quotes WHERE submit_time >= :poll_last } {
		puthelp "PRIVMSG $poll_target :Quote #$id was submitted by $nick: $translations(baseurl)$id"
	}

	set poll_last [clock seconds]
}

if {$poll_interval != ""} {
	set poll_last [clock seconds]
	bind cron - $poll_interval ${ns}::poll_new
	
}

# open the database for the first time
reopen_db

}
